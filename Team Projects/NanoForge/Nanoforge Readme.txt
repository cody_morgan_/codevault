These scripts represent the work that I contributed to the game Nanoforge. As this is a game with ongoing development I cannot 
post the complete game. I worked to integrate several existing scripts and systems both with my own work and with other existing work.
I also worked to update the existing game from an older version of unity into the latest version.

I created the following systems: 
* Fuel Management
	- Manages the fuel system that feeds the crafting system. Each object has a recipe using 4 fuels, which act as a sort of ammo.
	- Generates said recipe and is intregated with Inventory Master Asset and existing game structure
* Item Management (LootManager.cs)
	- Generates and manages procedurally generated consumable items
	- Generates and manages blueprint items for crafting
* Meter System (HUDScript.cs)
	- Reconfigured existing script to work with new UI
	- Improved user experience by simplifying and streamlining UI
	- Added fuel meters to HUD




