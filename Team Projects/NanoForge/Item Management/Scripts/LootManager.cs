﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

public class LootManager : MonoBehaviour {
	public TextAsset lootData;
	public string openLoot = "e";
	public int[] lootQuantityRange = new int[]{1,5};
	public GameObject player;
    //set this to globally change all containers that are otherwise not set
	public int distanceToOpenStorage = 2;
	public GameObject storagePrefab;

	List<string> suitableTags=new List<string>(); 
	string[][] lootMatrix = new string[4][];
	XmlDocument xmlDoc = new XmlDocument();
	ItemDataBaseList itemDataBase;

	void Start()
	{
		xmlDoc.LoadXml(lootData.text);
		itemDataBase = (ItemDataBaseList)Resources.Load ("ItemDatabase");
		if (isActiveAndEnabled) {
			suitableTags=ReadLootData ("tags","name");
			BuildLootMatrix ();
			BuildInventories (suitableTags);
		}
	}
	void Update()
	{
		Ray ray = Camera.main.ScreenPointToRay (new Vector3(Input.mousePosition.x,Input.mousePosition.y,25f));
		RaycastHit hit;
		Physics.Raycast (ray, out hit);
		Vector3 mousePos =Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,hit.distance));
		Debug.DrawRay (player.transform.position, mousePos);
		if (Input.GetKeyUp (openLoot) && hit.collider != null) {
			if (suitableTags.Contains(hit.collider.tag)) {
				Debug.Log (hit.collider.name + Vector3.Distance (player.transform.position, mousePos));
				GenerateLoot (hit);
			} else
				Debug.Log ("unsuitable tag or distance " + hit.collider.tag + " " + Vector3.Distance (player.transform.position, mousePos));
		}
	}

	//finds and returns list of adjectives under the group name, also used for suitable tag list creation
	public List<string> ReadLootData(string groupName, string adjectiveName)
	{
		XmlNodeList tagList = xmlDoc.GetElementsByTagName(groupName); //Finds the list of tags that are considered to be Lootable
		List<string> xmlList = new List<string>();
		foreach (XmlNode tag in tagList){
			XmlNodeList loots = tag.ChildNodes;
			foreach(XmlNode loot in loots){
				if (loot.Name == adjectiveName){
					xmlList.Add(loot.InnerText);
					//Debug.Log (loot.InnerText);
				}
			}
		}
		return xmlList;
	}
		
	//create inventories for all suitable tags
	void BuildInventories(List<string> tags)
	{
		//tag all containers under "LootParent" parent
		GameObject[] lootParents = GameObject.FindGameObjectsWithTag ("LootParent");
		foreach (GameObject parent in lootParents) {
			Transform[] children = parent.GetComponentsInChildren<Transform> ();
			foreach (Transform child in children) {
				if (child.tag != "LootParent")
					child.tag="Lootable";
			}
		}
		//create inventories for all containers
		foreach (string tag in tags) {
			GameObject[] containers = GameObject.FindGameObjectsWithTag (tag);
			foreach (GameObject container in containers) {
				if (container.gameObject.GetComponent<StorageInventory> () == null) {
					container.gameObject.AddComponent<StorageInventory> ();
                    if (container.GetComponent<StorageInventory>().distanceToOpenStorage == 0)
					    container.GetComponent<StorageInventory> ().distanceToOpenStorage = distanceToOpenStorage;
					GameObject newInventory = (GameObject)Instantiate (storagePrefab);
					newInventory.name = container.name + " Inventory";
					newInventory.transform.parent = GameObject.Find ("Inventories").transform.GetChild(0).transform;
					newInventory.transform.SetAsFirstSibling ();
					container.GetComponent<StorageInventory> ().inventory = newInventory;
				}
			}
		}
	}

	//converts xml config to string matrix
	void BuildLootMatrix()
	{
		for (int i = 0; i < lootMatrix.Length; i++) {
			string[] adjectives = new string[]{ "sizes", "adjectives", "flavors", "items", "size", "adjective", "flavor", "item" };
			List <string> adjectiveList = ReadLootData (adjectives [i], adjectives [i + 4]);
			lootMatrix [i] = adjectiveList.ToArray ();
		}
	}

	//generates a text representation of loot
	void GenerateLoot(RaycastHit hit)
	{
		string[] lootTotal=null;

		//construct an array of loot
		for (int quantity = Random.Range (lootQuantityRange [0], lootQuantityRange [1]); quantity > 0; quantity--)
		{
			if (lootTotal==null)
				lootTotal = new string[quantity];

			//contruct one piece of loot
			string loot = "";
			for (int i = 0; i<lootMatrix.Length; i++) {
				int rand = Random.Range (0, lootMatrix[i].Length-1);
				loot += lootMatrix [i][rand];
				if (i<3)
					loot+=" ";
			}
			lootTotal [quantity-1] = loot;
			Debug.Log (loot);
		}
		foreach (string loot in lootTotal) {
			//make a new item if necessary
			string[] adjectives = loot.Split (' ');
			int itemID = CreateItem (adjectives);
			//add loot with itemID
			hit.collider.GetComponent<StorageInventory>().inventory.GetComponent<Inventory>().addItemToInventory(itemID,1);
		}

		hit.collider.tag ="Looted";
	}

    //generates an Inventory Master item for consumable items
	public int CreateItem (string[] itemDescription) {

        //checks if the item is already in the item database
        Item currentItem = itemDataBase.getItemByName (itemDescription [1]+ " " + itemDescription [3]);
		if (currentItem == null) {
			Sprite icon = null;
			Sprite iconOverlay = null;
			//build the path to 'Sprites' folder
			string iconPath = AssetDatabase.GetAssetPath (GetComponent<LootManager> ().lootData);
			for (int i = iconPath.Length - 1; i >= 0; i--) {
				if (iconPath [i] == '/') {
					iconPath = iconPath.Substring (0, i) + "/Sprites";
					break;
				}
			}
			string[] iconDirectory = Directory.GetFiles (iconPath);
			//Gets rid of capitalization for standard comparison
			for (int i = 0; i < iconDirectory.Length; i++) {
				iconDirectory [i] = iconDirectory [i].ToLower ();
				try {
					itemDescription [i] = itemDescription [i].ToLower ();
				} catch {}
			}
			//Only checks the 2nd and last entries because the size and color (at 0 and 2) are taken into account differently than the adjective and item
			for (int i = 1; i <= 3; i += 2) {
				foreach (string path in iconDirectory) {
					if (path.Contains (itemDescription [i]) && !path.Contains (".meta")) {
						if (i == 1)
							iconOverlay = (Sprite)(AssetDatabase.LoadAssetAtPath (path, typeof(Sprite)));
						if (i == 3)
							icon = (Sprite)(AssetDatabase.LoadAssetAtPath (path, typeof(Sprite)));
					}
				}
			}

            float[] itemEffects = GenerateEffect(itemDescription);

            Item item = new Item {
                // the name is the adjective + item ex: "moldy poptart"

                itemName = itemDescription[1] + " " + itemDescription[3],
                itemID = itemDataBase.itemList.Count + 1,
                itemDesc = itemDescription[0] + " " + itemDescription[1] + " " + itemDescription[2] + " " + itemDescription[3],
                itemIcon = icon,
                itemIconOverlay = iconOverlay,
                itemValue = 1,
                maxStack = 3,
                itemType = ItemType.Consumable,
                meterAmounts = itemEffects
			};
			itemDataBase.itemList.Add (item);
			return item.itemID;
		} 
		return currentItem.itemID;
	}

    //generates items for fuel recipes when calling without fuel recipe
    public void CreateItem(GameObject gameobject)
    {
        FuelManager fuelManager = GameObject.FindObjectOfType<FuelManager>();
        CreateItem(gameobject, new int[] {
            fuelManager.BuildAestheticFuel(gameobject),
            fuelManager.BuildPhysicsFuel(gameobject),
            fuelManager.BuildMatterFuel(gameobject),
            fuelManager.BuildTransformFuel(gameobject)
        });
    }

    //generates items for fuel recipies when calling with the fuel recipe
    public int CreateItem(GameObject gameobject, int[] fuelRecipe)
    {

        //checks if the item is already in the item database
        Item currentItem = itemDataBase.getItemByName(gameobject.name);
        if (currentItem == null)
        {
            //builds item description as the fuel requirements for the item
            List<string> fuelNames = new List<string>();
            fuelNames.Add("Aesthetic");
            fuelNames.Add("Physics");
            fuelNames.Add("Matter");
            fuelNames.Add("Transform");
            string itemDescription = "Requires: ";
            foreach (float fuel in fuelRecipe)
            {
                itemDescription += fuel.ToString() + " "+fuelNames[0] + " Fuel, ";
                fuelNames.RemoveAt(0);                
            }

            Item item = new Item
            {
                itemName = gameobject.name,
                itemID = itemDataBase.itemList.Count + 1,
                itemDesc = itemDescription,
                itemModel = gameobject.gameObject,
                itemValue = 1,
                maxStack = 1,
                fuelRecipe = fuelRecipe
        };
            itemDataBase.itemList.Add(item);
            return item.itemID;
        }
        return currentItem.itemID;
    }

    //generates an array of effects for a given item
    float[] GenerateEffect(string[] item)
	{
        //a string list of all the effects of a given item
		List<string> effectList = new List<string> ();

        //loads all effects into string list
        for (int i = 0; i < 4; i++)
		{
			string[] splitMultipleEffects = xmlDoc.GetElementsByTagName (item [i]).Item (0).InnerText.Split (' ');
			foreach(string effect in splitMultipleEffects)
				effectList.Add(effect);
		}

		//adds global changes to string effect list
		XmlNodeList globalChanges = xmlDoc.GetElementsByTagName ("global").Item (0).ChildNodes;
		foreach (XmlNode node in globalChanges) {
			if(!node.Name.Contains("comment"))
				effectList.Add (node.Name + "+" + node.InnerText);
		}
			
		string[] attributes = new string[]{ "charge", "sanity", "hunger", "health" };

        //each array represents a meter. the values in the array correspond to the amount of change and the global change
		float[][] changes=new float[attributes.Length][];

        for (int i = 0; i < changes.Length; i++)
        {
            //[adjectives sum][global]
            changes[i] = new float[2] { 0, 0 };
        }

		//converts string data to usable float data
		for (int i = 0; i < attributes.Length; i++)
        {
			foreach (string effect in effectList)
            {
                //finds all effects that are either "all=##", "all=rand", "adjective=rand", "adjective=##". It adds these effects to changes[i][0]
                if (effect.Contains("="))
                {
                    float[] randomRange = new float[] { 0, 0 };
                    string[] changeAmount = effect.Split('=');

                    //creates random range for random effects 
                    if (effect.Contains("=rand"))
                    {
                        //must be formatted as such: all=rand(min,max)
                        char[] splitChars = new char[] { '(', ')', ',' };
                        string[] randomRangeTemp = effect.Split(splitChars);
                        randomRange[0] = float.Parse(randomRangeTemp[1]);
                        randomRange[1] = float.Parse(randomRangeTemp[2]);
                    }

                    //addresses the effects that change all meters
                    if (effect.Contains("all="))
                    {
                        float changeAllResult = 0;
                        float.TryParse(changeAmount[1], out changeAllResult);

                        for (int c = 0; c < changes.Length; c++)
                        {
                            changes[c][0] += changeAllResult + Random.Range(randomRange[0], randomRange[1]);
                        }
                    }

                    else if (effect.Contains(attributes[i]))
                    {
                        //addresses random changes to only one meter
                        if (changeAmount[1].Contains("=rand"))
                            changes[i][0] += Random.Range(randomRange[0], randomRange[1]);

                        //finds any adjectives that are left. These are the majority of effects
                        else
                            changes[i][0] += float.Parse(changeAmount[1]);
                    }
                }

                //finds global changes and adds them to changes[i][1]
                else if (effect.Contains("+") && effect.Contains(attributes[i]))
                {
                    string[] effectSplit = effect.Split('+');
                    changes[i][1] += float.Parse(effectSplit[1]);
                }
			}
		}

        //Here's the magic formula : size(adjective + flavor + item)+global
        float[] itemEffects = new float[4];
        for (int i = 0; i < itemEffects.Length; i++)
            itemEffects[i] = (float.Parse(effectList[0]) * changes[i][0]) + changes[i][1];
        return itemEffects;
	}
}