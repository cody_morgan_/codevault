﻿using UnityEngine;
using System.Collections;

public class CoinSpacing : MonoBehaviour {
	public GameObject coinSpacer;
	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "CoinSpace" && col.gameObject.transform.position.y < 4.5) {
			float hieght =0;
			for (float i = 4.666f;transform.position.y < i;i-=2.333f)
				hieght = i;
			Vector3 location = new Vector3 (transform.position.x,hieght,0);
			Instantiate (coinSpacer,location,Quaternion.identity);
			if (col.gameObject.name != "Release" && col.gameObject.name != "Release(Clone)")
				col.collider.gameObject.tag = "CoinSpaceBottom";
		}
	}
}
