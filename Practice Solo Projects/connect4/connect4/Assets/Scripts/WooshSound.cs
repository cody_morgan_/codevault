﻿using UnityEngine;
using System.Collections;

public class WooshSound : MonoBehaviour {
	public AudioClip woosh;
	public CameraRotate cameraScript;
	bool playWoosh=true;

	AudioSource audioa;
	public void Woosh ()
	{
		audioa = GetComponent<AudioSource> ();
		StartCoroutine(WooshTiming ());
	}
	IEnumerator WooshTiming()
	{
		if (!audioa.isPlaying && cameraScript.isRotating && playWoosh)
			audioa.PlayOneShot (woosh, 0.2f);
		playWoosh = false;
		yield return new WaitForSeconds(2.0f);
		playWoosh = true;
	}
}
