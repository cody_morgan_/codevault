﻿using UnityEngine;
using System.Collections;

public class CameraRotate : MonoBehaviour {
	public float cameraRotateSpeed = 100.0f;
	public bool isRotating = false;
	public WooshSound WooshScript;

	public void CameraInitial(int player)
	{
		if (player % 2 == 0) {
			this.transform.eulerAngles = new Vector3(
				this.transform.eulerAngles.x+20,
				this.transform.eulerAngles.y,
				this.transform.eulerAngles.z);
			this.transform.position = new Vector3 (0, 10, -25);
		} 
		else {
			this.transform.eulerAngles = new Vector3(
				this.transform.eulerAngles.x+20,
				this.transform.eulerAngles.y+180,
				this.transform.eulerAngles.z);
			this.transform.position = new Vector3 (0, 10, 25);
		}
	}
	public void StartRotate(bool rotating)
	{
		isRotating = rotating;
		WooshScript.Woosh ();
	}
	public void FixedUpdate()
	{
		if (isRotating)
			this.transform.RotateAround (Vector3.zero, Vector3.up, cameraRotateSpeed * Time.deltaTime);
		isRotating = false;
	}
}