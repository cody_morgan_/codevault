﻿using UnityEngine;
using System.Collections;

public class DeathTouch : MonoBehaviour {
	void OnCollisionEnter (Collision col)
	{
		Destroy (col.gameObject);
	}
}