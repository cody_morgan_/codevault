Version 2.0
Design:
	* This is designed to be a tool to help me learn how to code specifically focusing on 3D manipulation and android publishing
	* Connect 4, as a concenpt, does not need to be 3D or involve 3D rotation - however I wanted to branch out into the next dimension  
	* This currently is a 2 player game for either PC or android (one the same device)

How To Play:
	* Tap {android} or click {PC} area above connect 4 board to spawn game chip (red or blue)
	* The board will automatically spin to the other side to signify that it is the other player's turn
	* If a player connects 4 game chips in a row either vertically, horizontally, then that player wins
	* A screen will fade in asking to play again, simply tap it to restart the game

Known Issues:
	* Coins can bounce off of game board into the wrong slot thereby misleading the player
	* {Android} Restart button is too small

To do:
	* Add AI support
