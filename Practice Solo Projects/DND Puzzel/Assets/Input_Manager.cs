﻿using UnityEngine;
using System.Collections;

public class Input_Manager : MonoBehaviour {
    public float turnSpeed;

    Ray ray;
    RaycastHit hit;
    bool rotate;
    Vector3 oldMousePos = Vector3.zero;
	void Start ()
    {
        turnSpeed = 10f;
        hit = new RaycastHit();
        rotate = false;
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);
            Debug.Log(hit.transform.name);
            rotate = true;
            oldMousePos = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
            rotate = false;
        if (rotate && hit.transform.name != "floor")
            RotateObject(hit.transform);
        if(rotate && hit.transform.name == "floor")
            RotateObject(GameObject.Find("base mesh").transform);
    }
    void RotateObject(Transform rotatingObj)
    {
        if (rotatingObj.tag == "rotatable")
        {
            Vector3 neededEuler = new Vector3(rotatingObj.parent.localRotation.x, 360 * ((oldMousePos.x - Input.mousePosition.x) / Screen.width), rotatingObj.parent.localRotation.z);
            if (rotatingObj.name.Contains("shield"))
                neededEuler.y = Mathf.Clamp(neededEuler.y, -90, -40);
            if (rotatingObj.name.Contains("wing"))
                neededEuler = new Vector3(rotatingObj.parent.localRotation.x, rotatingObj.parent.localRotation.y, Mathf.Clamp(360 * ((oldMousePos.x - Input.mousePosition.x) / Screen.width), -50, 0));

            rotatingObj.parent.localRotation = Quaternion.Slerp(rotatingObj.parent.localRotation, Quaternion.Euler(neededEuler), Time.deltaTime * turnSpeed);
        }
    }
}
