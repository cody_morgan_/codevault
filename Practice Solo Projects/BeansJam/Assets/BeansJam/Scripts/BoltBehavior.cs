﻿using UnityEngine;
using System.Collections;

public class BoltBehavior : MonoBehaviour
{

    public float speed;
    public float timeToLive;
    public int damage;


    float spawnTime;
    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
        spawnTime = Time.time;
        timeToLive = 2.0f;
        damage = 10;
    }
    void Update()
    {
        if (Time.time > spawnTime + timeToLive)
            Destroy(gameObject);
            
    }
    void OnCollisionEnter(Collision col)
    {
        Destroy(gameObject);
        if (col.transform.tag == "Player")
        {
            Debug.Log("You were hit!");
            GameObject.Find("GameManager").GetComponent<GameManager>().PlayerDamage(damage);
        }
        else
        {
            if (col.transform.tag == "Cargo")
            {
                Debug.Log("You damaged cargo ship");
                col.gameObject.GetComponent<CargoBehavior>().health -= damage;
            }
            else
            {
                if (col.transform.tag == "Police")
                {
                    Debug.Log("You damaged police");
                    col.gameObject.GetComponent<PoliceBehavior>().health -= damage;
                }
                else
                {
                    Debug.Log("You hit a planet or something");
                    
                }
            }
        }
    }
}
