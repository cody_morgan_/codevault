﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public KeyCode forward;
    public KeyCode back;
    public KeyCode right;
    public KeyCode left;
    public KeyCode up;
    public KeyCode down;
    public KeyCode fire;
    public KeyCode rollRight;
    public KeyCode rollLeft;
    public float speed;
    public float turningResistance;
    public GameObject bolt;
    public float shootDelay;

    KeyCode[] keybindings;
    Vector3[] movement;
    float nextShot;

    void Start ()
    {
        forward = KeyCode.W;
        back = KeyCode.S;
        right = KeyCode.D;
        left = KeyCode.A;
        up = KeyCode.E;
        down = KeyCode.Q;
        rollRight = KeyCode.Mouse1;
        rollLeft = KeyCode.Mouse0;
        fire = KeyCode.Space;
        keybindings = new KeyCode[] {
            KeyCode.W, KeyCode.S, KeyCode.D, KeyCode.A,
            KeyCode.E, KeyCode.Q,
            KeyCode.Mouse0, KeyCode.Mouse1,
            KeyCode.Space };
        nextShot = 0;
        shootDelay = 0.1f;
        turningResistance = 3;
    }

	void Update ()
    {
        if (Input.GetKey(fire))
            FindObjectOfType<Fade>().fade = false;
        if  (!FindObjectOfType<Fade>().fade)
        { 
            Cursor.lockState = CursorLockMode.Locked;
            var x = Input.GetAxis("Mouse X");
            var y = Input.GetAxis("Mouse Y");
            GetComponent<Rigidbody>().AddRelativeTorque(y / turningResistance, x / turningResistance, 0);

            movement = new Vector3[] {
                new Vector3(0,0,speed), new Vector3(0,0,-speed), new Vector3(speed,0,0), new Vector3(-speed,0,0),
                new Vector3(0,speed,0), new Vector3(0,-speed,0) };
            if (Input.anyKey == true)
            {
                if (Input.GetKey(fire) && Time.time > nextShot + shootDelay)
                {
                    GetComponent<AudioSource>().Play();
                    nextShot = Time.time;
                    GameObject laser = Instantiate(bolt);
                    laser.transform.parent = this.transform;
                    laser.transform.localPosition = new Vector3(0, 0, 2);
                    laser.transform.rotation = transform.rotation;
                }
                else
                {
                    int index = 0;
                    for (int key = 0; key < keybindings.Length; key++)
                    {
                        if (Input.GetKey(keybindings[key]))
                        {
                            if (key == 6)
                                GetComponent<Rigidbody>().AddRelativeTorque(0, 0, 1);
                            else
                            {
                                if (key == 7)
                                    GetComponent<Rigidbody>().AddRelativeTorque(0, 0, -1);
                                else
                                {
                                    GetComponent<Rigidbody>().AddRelativeForce(movement[index] * Time.deltaTime);
                                    FindObjectOfType<GameManager>().GetComponent<GameManager>().Fuel();
                                }
                            }
                        }
                        index++;
                    }
                }
            }
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.name == "Hideout")
        {
            Debug.Log("hideout");
            FindObjectOfType<GameManager>().GetComponent<GameManager>().LevelChange();
        }
        else
        {
            if (col.transform.tag == "Planet")
            {
                FindObjectOfType<GameManager>().PlayerDamage(5);
            }
        }
    }
}
