﻿using UnityEngine;
using System.Collections;

public class CollisionDetection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "WayPoint" || col.gameObject.tag == "Planet")
        {
            //prevent planets from spawning on one another
            transform.localPosition = new Vector3(transform.localPosition.x + 10, transform.localPosition.y + 10, transform.localPosition.y + 10);
        }
    }
}
