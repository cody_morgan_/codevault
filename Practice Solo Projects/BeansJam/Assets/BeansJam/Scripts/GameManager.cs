﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public Text score;
    public Slider healthMeter;
    public Slider fuel;
    public int cargoCount;          //how many cargo ships spawn per level
    public int policeCount;         //police per level
    public int size;
    public Transform planets;
    public Transform waypoints;
    public GameObject policeShip;
    public GameObject cargoShip;
    public GameObject explosion;

    int level;
    bool dead;
    bool fade;

    void Start()
    {
        level = 1;
        cargoCount = 3;
        policeCount = 1;
        size = 200;
        dead = false;
        fade = false;
        LevelStart();
    }
    void Update()
    {
        if ((healthMeter.value <= 0 || fuel.value <= 0) && !dead)
            PlayerDeath();
        if (fade)
        {
            foreach (Transform child in GameObject.Find("Death Screen").transform)
            {
                Color curColor = Color.green;
                try
                {
                    curColor = child.GetComponent<Image>().color;
                    curColor.a = Mathf.Lerp(curColor.a, 1.0f, 0.5f * Time.deltaTime);
                    child.GetComponent<Image>().color = curColor;
                }
                catch (System.Exception)
                {
                    curColor = child.GetComponent<Text>().color;
                    curColor.a = Mathf.Lerp(curColor.a, 1.0f, 0.5f * Time.deltaTime);
                    child.GetComponent<Text>().color = curColor;
                    throw;
                }

            }
        }
    }

    public void NpcDeath(int money)
    {
        int currentScore = int.Parse(score.GetComponent<Text>().text.Substring(7));
        currentScore += money * level;
        score.GetComponent<Text>().text = "Score: " + currentScore;
    }
    public void PlayerDamage(int damage)
    {
        healthMeter.value -= damage;
    }
    public void Fuel()
    {
        fuel.value -= 0.05f;
    }
    public void LevelChange()
    {
        healthMeter.value = 100;
        fuel.value = 100;
        FindObjectOfType<Fade>().fade = true;
        foreach (GameObject ship in GameObject.FindGameObjectsWithTag("Police"))
        {
            Destroy(ship);
        }
        foreach (GameObject ship in GameObject.FindGameObjectsWithTag("Cargo"))
        {
            Destroy(ship);
        }
        level++;
        Debug.Log(level);
        LevelStart();
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameObject.Find("Death Screen").transform.GetChild(3).gameObject.SetActive(false);
    }
    void PlayerDeath()
    {
        //youre dead
        dead = true;
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;
        Instantiate(explosion, player.position,new Quaternion(0,0,0,0));
        GameObject.FindGameObjectWithTag("MainCamera").transform.parent = transform;
        Destroy(player.gameObject);
        StartCoroutine(DeathScreen(2.0f));
        GameObject.Find("Death Screen").transform.GetChild(3).gameObject.SetActive(true);    
    }
    void LevelStart()
    {
        Create(planets);
        Create(waypoints);   
        for (int i = 0; i < level * cargoCount; i++)
        {
            Instantiate(cargoShip);
        }
        for (int i = 0; i < level * policeCount; i++)
        {
            Instantiate(policeShip);

        }
    }
    void Create(Transform objParent)
    {
        foreach (Transform child in objParent)
        {
            int magnify = Random.Range(5, 10);
            child.localScale = new Vector3(magnify * child.transform.localScale.x, magnify * child.transform.localScale.y, magnify * child.transform.localScale.z);
            child.localPosition = new Vector3(Random.Range(-size, size), Random.Range(-size, size), Random.Range(-size, size));
        }
    }
    IEnumerator DeathScreen(float time)
    {
        yield return new WaitForSeconds(time);
        fade = true;
    }

}
