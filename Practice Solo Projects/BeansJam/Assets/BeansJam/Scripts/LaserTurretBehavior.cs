﻿using UnityEngine;
using System.Collections;

public class LaserTurretBehavior : MonoBehaviour {
    public float turnSpeed;
    public float shootDelay;
    public float damage;
    public GameObject bolt;

    float nextShot;
	// Use this for initialization
	void Start ()
    {
        turnSpeed = 1;
        shootDelay = 5;
        nextShot = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;
        Debug.DrawLine(transform.position, player.position);
        Quaternion neededRotation = Quaternion.LookRotation(player.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, neededRotation, Time.deltaTime * turnSpeed);

        if (Time.time > nextShot + shootDelay)
        {
            GetComponent<AudioSource>().Play();
            nextShot = Time.time;
            GameObject laser = Instantiate(bolt);
            laser.transform.parent = this.transform;
            laser.transform.localPosition = new Vector3(0,0,1);
            laser.transform.rotation = transform.rotation;
        }
    }

}
