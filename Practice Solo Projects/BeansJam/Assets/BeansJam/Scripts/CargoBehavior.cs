﻿using UnityEngine;
using System.Collections;


public class CargoBehavior : MonoBehaviour
{
    public float speed;
    public float turningSpeed;
    public float shootingDelay;
    public float health;
    public int money;
    public GameObject explosion;

    Transform planets;
    Transform waypoints;
    int nextStop = 0;
    Vector3[] route;
    // Use this for initialization
    void Start ()
    {
        planets = GameObject.Find("Planets").transform;
        waypoints = GameObject.Find("Waypoints").transform;
        route = GenerateRoute();
        health = 1000;
        money = 100;
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, route[nextStop], speed * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(route[nextStop] - transform.position);
        if (health <= 0)
            Death();
    }

    //moving behavior
    Vector3[] GenerateRoute()
    {
        Vector3[] route;
        int home = Random.Range(0, planets.childCount);
        int destination = Random.Range(0, planets.childCount); ;
        while (destination == home)
        {
            destination = Random.Range(0, planets.childCount);
        }
        //start home, find waypoints, go to destination
        transform.position = planets.GetChild(home).GetChild(0).position;
        Debug.Log("home: " + transform.position);
        int waypointTotal = Random.Range(0, waypoints.childCount);
        route = new Vector3[waypointTotal + 1];
        for (int waypoint = 0; waypoint < waypointTotal; waypoint++)
        {
            route[waypoint] = waypoints.GetChild(Random.Range(0, waypoints.childCount)).position;
        }
        route[route.Length - 1] = planets.GetChild(destination).position;
        foreach (Vector3 vector in route)
        {
            Debug.Log(vector);
        }
        return route;
    }
    void OnCollisionEnter(Collision col)
    {
        if (route != null)
        {
            if (col.gameObject.tag == "WayPoint" && Distance(transform.position, route[nextStop]) < 10f ||
               (col.gameObject.tag == "Planet" && nextStop == route.Length - 1))
            {
                nextStop++;
                money -= money / route.Length;
                if (nextStop >= route.Length)
                {
                    Destroy(gameObject);
                }
                else
                    Debug.Log("target reached, next stop: " + route[nextStop]);
            }
        }
            
    }
    void NextStop()
    {
        
        
    }
    float Distance(Vector3 ship, Vector3 planet)
    {
        float distance =
            Mathf.Sqrt(
            ((ship.x - planet.x) * (ship.x - planet.x)) +
            ((ship.y - planet.y) * (ship.x - planet.x)) +
            ((ship.z - planet.z) * (ship.x - planet.x)));
        return distance;
    }
    void Death()
    {
        GameObject.Find("GameManager").GetComponent<GameManager>().NpcDeath(money);
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
        
    }

}
