﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fade : MonoBehaviour {
    public bool fade;

    Color curColor;
    Color curColorText;
    // Use this for initialization
    void Start () {
        fade = false;
        curColor = Color.green;
        curColorText = Color.white;
    }
	
	// Update is called once per frame
	void Update () {

        if (fade)
        {
            curColor = transform.GetComponentInChildren<Image>().color;
            curColorText = transform.GetComponentInChildren<Text>().color;
            curColor.a = Mathf.Lerp(curColor.a, 0.75f, 0.5f * Time.deltaTime);
            curColorText.a = Mathf.Lerp(curColor.a, 0.75f, 0.5f * Time.deltaTime);
            transform.GetComponentInChildren<Image>().color = curColor;
            transform.GetComponentInChildren<Text>().color = curColorText;
        }
        else
        {
            curColor = transform.GetComponentInChildren<Image>().color;
            curColorText = transform.GetComponentInChildren<Text>().color;
            curColor.a = 0;
            curColorText.a = 0;
            transform.GetComponentInChildren<Image>().color = curColor;
            transform.GetComponentInChildren<Text>().color = curColorText;
        }
    }
}
