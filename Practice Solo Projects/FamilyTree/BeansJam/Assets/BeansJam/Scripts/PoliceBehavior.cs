﻿using UnityEngine;
using System.Collections;

public class PoliceBehavior : MonoBehaviour {
    public float speed;
    public float turnSpeed;
    public float shootingDelay;
    public float health;
    public int money;
    public GameObject explosion;

    // Use this for initialization
    void Start ()
    {
        health = 200;
        money = 50;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;
        Quaternion neededRotation = Quaternion.LookRotation(player.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, neededRotation, Time.deltaTime * turnSpeed);
        transform.position = Vector3.MoveTowards(transform.position, player.GetChild(0).position, speed * Time.deltaTime);

        if (health <= 0)
            Death();

    }
    void Death()
    {
        GameObject.Find("GameManager").GetComponent<GameManager>().NpcDeath(money);
        Destroy(gameObject);
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
