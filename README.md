# Purpose #
The purpose of the repository is to demonstrate the growth and evolution of my programming abilities. There are games that I have worked on by myself (solo games) as well as ones in which I worked on a team. Once I start my formal education at digipen I will add more folders for those projects as they will likely be of a higher quality.

# Organization #
All games are hosted under the main branch and are separated based on my role.
All other projects are hosted on a Programs branch that matches the programming language (e.g "C# programs") 
